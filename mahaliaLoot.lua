local MahaliaLoot = {}

--parse the link to get colour, data and text from it.
function ZO_LinkHandler_ParseLink(link)
    if type(link) == "string" then
        local color, data, text = link:match("|H(.-):(.-)|h(.-)|h")
        return text, colour, zo_strsplit(':', data)
    else
    return null

    end
end

-- On item update get the contents of the item and spit it out to the chat window
function MahaliaLoot:OnItemGain(eventCode, recievedBy, itemName,  quantity, itemSound, lootType, self)
-- only worry about items the player gets.
    if self then
        itemName = zo_strformat(SI_TOOLTIP_ITEM_NAME, itemName)
        CHAT_SYSTEM:AddMessage(string.format("You looted %s %s",quantity, itemName))
    end
end

--EVENT REGISTRATION
EVENT_MANAGER:RegisterForEvent("MahaliaLoot", EVENT_LOOT_RECEIVED, function(...) MahaliaLoot:OnItemGain(...) end)






